import os, sys
params_v2= {   'chromedriver': os.path.join(os.path.dirname(__file__), 'chromedriver'),          # chromedriver binary location
            'window': (1600, 900),                                                            # window size in !headless mode
            'headless': False,                                                                # ChromeDriver mode
            'v_weights':  [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1,
                            3.3, 3.5, 3.7, 3.9, 4.1, 4.3, 4.5, 4.7, 4.9, 5.1 ],
            'dv_weights': [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1,
                            3.3, 3.5, 3.7, 3.9, 4.1, 4.3, 4.5, 4.7, 4.9, 5.1 ],
            'ddv_weights': [ 6, 6, 6, 5, 5, 5, 4, 4, 4, 3,
                             3, 3, 2, 2, 2, 1, 1, 1, 1, 1,
                             1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                             1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                             1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1,
                             3.3, 3.5, 3.7, 3.9, 4.1, 4.3, 4.5, 4.7, 4.9, 5.1],
            'neuro': {'nn_isize': 62,          # NN input size ([:nn_isize] - train, [-nn_isize:] - predict)
                      'vector_size': 140,       # Size of period(s) to collect data
                      'hid_size': 100,          # NN hidden layer size
                      'hid_count': 2,           # NN hidden layers count
                      'epochs':10,              # Epoch count (for NN training)
                      'correction':0.25,         # Probability of deal on negative predicate (for variation)
                      'deal_prob':0.9,          # Probability of deal if all conditions met
                      'backcount':120,           # Amount of ticks with doh>inc
                      'backcount_corr':0.51}}    # At least backcount_corr of 1 should be true (see above)


params = {  'chromedriver': os.path.join(os.path.dirname(__file__), 'chromedriver'),          # chromedriver binary location
            'window': (1600, 900),                                                            # window size in !headless mode
            'headless': False,                                                                # ChromeDriver mode
            'neuro': {'vector_size': 150,       # Size of period(s) to collect data
                      'hid_size': 12,          # NN hidden layer size
                      'hid_count': 2,           # NN hidden layers count
                      'epochs':10,              # Epoch count (for NN training)
                      'min_threshold': 0.5,
                      'correction':0.1,         # Probability of deal on negative predicate (for variation)
                      'deal_prob':0.95}}          # Probability of deal if all conditions met


