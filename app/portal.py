#!/bin/env python3
# -*- coding: utf-8 -*-

import os, time
from selenium.webdriver import Chrome, ActionChains
from datetime import datetime
from selenium.webdriver.chrome import webdriver as chrome_webdriver
from params import params

## Get data from portal as dicts
class ScraperBot(object):
    def __enter__(self):
        return self
    
    def __exit__(self, *a):
        self.close_driver()
    
    ## Init chromedriver
    def __init__(self, indexdir=''):
        assert(params is not None and params != {})
        self.params=params

        opt = chrome_webdriver.Options()
        if self.params['headless']:
            opt.add_argument("--headless")
        
        self.driver = Chrome(self.params['chromedriver'], options=opt)
        self.driver.implicitly_wait(30)
        self.driver.set_window_size(self.params['window'][0], self.params['window'][1])

        print('Initialized ' + os.path.basename(self.params['chromedriver']) + ', headless=' + str(self.params['headless']))
    
    ## Destructor
    def close_driver(self):
        self.driver.quit()
        print('Closed ' + os.path.basename(self.params['chromedriver']))
        
    ## Press Button Down
    def btn_up(self):
        actions = ActionChains(self.driver)
        actions.send_keys("q")
        actions.perform()
        #self.driver.find_element_by_xpath(u'//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[5]/div/div/div/button[1]').click()

    ## Press Button Up
    def btn_down(self):
        actions = ActionChains(self.driver)
        actions.send_keys("r")
        actions.perform()
        #self.driver.find_element_by_xpath(u'//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[5]/div/div/div/button[2]').click()

        #self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='₽'])[79]/following::i[1]").click()
        #self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='₽'])[85]/following::i[1]").click()



    ## Login to Binomo
    def login_binomo(self):
        self.driver.get('https://binomo.com/ru/trading')
        self.driver.find_element_by_xpath('//*[@id="page_home_index"]/header/div[2]/div/div/div[2]/div/button').click()
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[1]/ui-n-input/div/input').send_keys("sadxui@gmail.com")
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[2]/ui-n-input/div/input').send_keys("Htgbyj86")
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[4]/button').click()

    def log_date(self):
        return str(datetime.now().strftime("[%d.%m-%H:%M:%S] "))

    ## Scrape report viewer for daystats, dict[measure_date]={daystats}
    def scrape_user_opinion(self):
        self.login_binomo()
        elem = self.driver.find_element_by_xpath('//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[4]/div/span[2]')
        acc = self.driver.find_element_by_css_selector('#page_home_trading > header > app-account > div > div.bar-item.x-offset-col-sm-l > div > button > div.b-unit-dropdown-balance.demo > app-currency > span > span > span.currency-amount.ng-binding')
        v_w = self.params['v_weights']
        dv_w = self.params['dv_weights']
        ddv_w = self.params['ddv_weights']
        assert(len(dv_w)==len(ddv_w))
        init_steps = len(dv_w)
        v_down = [0] * init_steps
        dv = [0] * init_steps
        ddv = [0] * init_steps
        time.sleep(5)
        while True:
            time.sleep(1)
            init_steps -= 1
            balance = int(float(acc.get_attribute("innerHTML").replace(' ', '').replace(',', '.')))
            v_down = v_down + [int(elem.get_attribute("innerHTML").replace(" ","").replace("%",""))]
            v_down_new=v_down[1:]
            v=[round(v_down_new[i-1]*v_w[i-1]) for i in range(1, len(v_down))]
            dv = [round((v_down[i] - v_down[i-1])*dv_w[i-1]) for i in range(1, len(v_down))]
            ddv = [round((dv[i] - dv[i - 1])*ddv_w[i-1]) for i in range(1, len(dv))]
            v_down=v_down_new
            #print(v_down)
            #print(dv)
            #print(ddv)
            if init_steps >= -5:
                print('Preparing')
                continue
            print("{}down={}; v_sum={:d}; dv_sum={:d}; ddv_sum={:d}".format(self.log_date(), v_down[len(v_down)-1], round(sum(v)/len(v)), sum(dv), sum(ddv)))
            #except:
                #continue
            continue

            if v_down >= 75 and v_down < 100:
                print(self.log_date() + "Placed DOWN on 75% (" + str(v_down) + ")")
                self.btn_down()
                time.sleep(170)
            elif v_down <= 25 and v_down > 0:
                print(self.log_date() + "Placed UP on 75% (" + str(100-v_down) + ")")
                self.btn_up()
                time.sleep(170)


bot = ScraperBot()
bot.scrape_user_opinion()
