#!/bin/env python3
# -*- coding: utf-8 -*-

import os, time, random, numpy, re
from selenium.webdriver import Chrome, ActionChains
from datetime import datetime, timedelta
from selenium.webdriver.chrome import webdriver as chrome_webdriver
from .params import params

#from keras.models import Sequential
#from keras.layers import Dense

# Get forecast based on user opinions for given time
class UserOpinionBot(object):
    def __enter__(self):
        return self
    
    def __exit__(self, *a):
        self.close_driver()
    
    # Init chromedriver and NN
    def __init__(self, indexdir=''):
        assert(params is not None and params != {})

        # Load parameters
        self.params=params

        # Init chromedriver with options
        opt = chrome_webdriver.Options()
        if self.params['headless']:
            opt.add_argument("--headless")
            #opt.add_argument('user-data-dir=/home/sadhui/.config/google-chrome/Default')

        self.driver = Chrome(self.params['chromedriver'], options=opt)
        self.driver.implicitly_wait(30)
        self.driver.set_window_size(self.params['window'][0], self.params['window'][1])
        print('Initialized ' + os.path.basename(self.params['chromedriver']) + ', headless=' + str(self.params['headless']))

        # Init and compile NN
        self.nn = Sequential()
        self.nn.add(Dense(self.params['neuro']['hid_size'], input_dim=8, activation='relu'))
        for i in range(1, self.params['neuro']['hid_count']):
            self.nn.add(Dense(self.params['neuro']['hid_count'], activation='relu'))
        self.nn.add(Dense(1, activation='sigmoid'))
        self.nn.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        print('Initialized NN')

    # Train NN with given X, Y
    def train_nn(self, dataX, dataY):
        X=[dataX['d1s'], dataX['d5s'], dataX['d15s'], dataX['d30s'], dataX['d1m'], dataX['exp_time'], dataX['mplus'], dataX['deal']]
        self.nn.fit(numpy.array([X]), numpy.array([dataY]), epochs=self.params['neuro']['epochs'], batch_size=1, verbose=0)

    # Get forecast of Y for given X
    def predict_nn(self, dataX):
        X=[dataX['d1s'], dataX['d5s'], dataX['d15s'], dataX['d30s'], dataX['d1m'], dataX['exp_time'], dataX['mplus'], dataX['deal']]
        res = self.nn.predict(numpy.array([X]))
        return res[0][0]

    # Destructor
    def close_driver(self):
        self.driver.quit()
        print('Closed ' + os.path.basename(self.params['chromedriver']))
        
    # Press Button Down
    def btn_up(self):
        actions = ActionChains(self.driver)
        actions.send_keys("q")
        actions.perform()

    # Press Button Up
    def btn_down(self):
        actions = ActionChains(self.driver)
        actions.send_keys("r")
        actions.perform()

    # Login to Binomo and get xpaths
    def init_binomo(self):
        self.driver.get('https://binomo.com/ru/trading')
        self.driver.find_element_by_xpath('//*[@id="page_home_index"]/header/div[2]/div/div/div[2]/div/button').click()
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[1]/ui-n-input/div/input').send_keys("sadxui@gmail.com")
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[2]/ui-n-input/div/input').send_keys("Htgbyj86")
        self.driver.find_element_by_xpath('//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[4]/button').click()

        self.driver.implicitly_wait(3)
        try:
            self.driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='EUR/USD (OTC)'])[1]/i[1]").click()
            self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='Доступные активы'])[2]/following::a[1]").click()
        except:
            pass
        time.sleep(6)
        self.dwn = self.driver.find_element_by_xpath('//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[4]/div/span[2]')
        self.bal = self.driver.find_element_by_xpath('//*[@id="page_home_trading"]/header/app-account/div/div[1]/div/button/div[1]/app-currency/span/span/span[1]')
        self.inv = self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='Всего инвестиций'])[1]/following::div[1]")
        self.doh = self.driver.find_element_by_xpath(u"(.//*[normalize-space(text()) and normalize-space(.)='Ожидаемый доход'])[1]/following::div[1]")
        self.vyp = self.driver.find_element_by_xpath('//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[3]/p[1]/span[2]')
        self.tim = self.driver.find_element_by_xpath('//*[@id="eur-graphs"]/div/div/ui-chart-container/div/div[1]/div/div/div[3]/binomo-countdown/div')

    # Update lists
    def update_values(self, l_dwn, l_bal, l_inv, l_doh, l_vyp):
        l_dwn = l_dwn[1:] + [int(self.dwn.get_attribute("innerHTML").replace(" ", "").replace("%", ""))]
        l_bal = l_bal + [int(float(self.bal.get_attribute("innerHTML").replace(' ', '').replace(',', '.')))]

        #try:
        val = re.search('\d+', self.inv.get_attribute("innerHTML").replace(' ','')).group(0)
        l_inv_last = int(val)
        #except Exception as ex:
            #self.log_date('ERROR parsing l_inv_last: ' + str(ex))
            #l_inv_last = l_inv[-1:][0]
        l_inv = l_inv + [l_inv_last]
        #try:
        val=re.search('\d+', self.doh.get_attribute("innerHTML").replace(' ','')).group(0)
        l_doh_last = int(val)
        #except Exception as ex:
            #self.log_date('ERROR parsing l_doh_last: ' + str(ex))
            #l_doh_last = l_doh[-1:][0]
        l_doh = l_doh[1:] + [l_doh_last]
        l_vyp = l_vyp[1:] + [int(self.vyp.get_attribute("innerHTML").replace(' ', '').replace('%', ''))]

        l_bal_diff = [l_bal[i]+l_inv[i]-l_bal[i-1]-l_inv[i-1] for i in range(1, len(l_bal))]
        l_bal = l_bal[1:]
        l_inv = l_inv[1:]

        tim = str(self.tim.get_attribute(("innerHTML")))
        tim = tim.split(':')
        tim = int(tim[0])*60+int(tim[1])
        deal_date = datetime.now() + timedelta(seconds=tim)
        deal_date = deal_date + timedelta(seconds=60-deal_date.second)
        before_deal = (deal_date - datetime.now()).seconds
        assert(before_deal>=0)
        return (l_dwn, l_bal, l_bal_diff, l_inv, l_doh, l_vyp, before_deal)

    # Print with timestamp
    def log_date(self, text):
        print(datetime.now().strftime("[%d.%m-%H:%M:%S] {}".format(text)))

    # Make deal on DOWN
    def make_deal_down(self):
        self.log_date('Deal on DOWN')
        self.btn_down()

    # Main training-predict cycle
    def scrape_user_opinion(self):
        self.init_binomo()
        l_dwn, l_bal, l_bal_diff, l_inv, l_doh, l_vyp = ([0] * self.params['neuro']['vector_size'] for _ in range(6))
        # Init list with prevruns results
        data = [None] * self.params['neuro']['vector_size']
        step = -1 * round(self.params['neuro']['vector_size']*1.3)
        self.log_date('Started train-predict cycle')

        while True:
            step += 1
            time.sleep(1)
            try:
                l_dwn, l_bal, l_bal_diff, l_inv, l_doh, l_vyp, exp_time = self.update_values(l_dwn, l_bal, l_inv, l_doh, l_vyp)
            except Exception as ex:
                self.log_date('ERROR: ' + str(ex))
                pass

            profit_made = 1 if (l_doh[-1:][0] > l_inv[-1:][0] or l_bal[-1:][0] > l_bal[-2:-1][0]) else 0

            if step == -1:
                self.log_date('Finished preparation phase')
            if step>0:
                data_item0 = {'d1s': l_dwn[-1:][0],
                              'd5s': round(numpy.mean(l_dwn[-5:])),
                              'd15s': round(numpy.mean(l_dwn[-15:])),
                              'd30s': round(numpy.mean(l_dwn[-30:])),
                              'd1m': round(numpy.mean(l_dwn[-60:])),
                              'exp_time': exp_time,
                              'mplus': profit_made,
                              'deal': 0}
                data_item1 = data_item0.copy()
                data_item1['deal'] = 1
                forecast0 = self.predict_nn(data_item0)
                forecast1 = self.predict_nn(data_item1)
                deal_made = 0

                if ((forecast1 > forecast0 and forecast1 > self.params['neuro']['min_threshold']) or \
                    (1 - random.random() >= 1 - self.params['neuro']['correction'])):
                    if random.random() > 1-self.params['neuro']['deal_prob']:
                        self.make_deal_down()
                        deal_made = 1
                        step = 0

                data[exp_time] = data_item1 if deal_made else data_item0

                # Aposteriori train
                print(data[0])
                print('profit_made={}', profit_made)
                if data[0] is not None:
                    self.train_nn(data[0], profit_made)

                # Shift training list <==1
                data = data[1:]+[None]

                #prediction = y_train
                self.log_date("step={:d}; down={:d}; f0={}; f1={}".format(step, l_dwn[-1:][0], forecast0, forecast1))

while True:
    try:
        bot = UserOpinionBot()
        bot.scrape_user_opinion()
    except Exception as ex:
        raise ex
        print(str(ex))
        if ex is KeyboardInterrupt:
            break
        time.sleep(20)
