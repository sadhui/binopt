#!/bin/env python3
# -*- coding: utf-8 -*-

from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.chrome import webdriver as wd
from selenium.webdriver.common.keys import Keys
from PIL import Image, ImageFilter
from datetime import datetime
from io import BytesIO
import time
import os, re, sys
import matplotlib.pyplot as plt
import pytesseract, numpy as np

params_v3 = {
    'browser': {
        'driver': os.path.join(os.path.dirname(__file__), 'chromedriver.exe'),  # Webdriver binary location
        'pattern': os.path.join(os.path.dirname(__file__), 'pattern.txt'),
        'window': (1920, 1080),  # window size in !headless mode
        'headless': True,                                                                  # Webdriver mode
        'wait': 10 },  # Delay for WebDriverWait
    'datatimer': {
        'enabled': False,
        'rate': 0.45, # Tick interval in seconds, default=1s
        'nop': 0.02 # Wait interval in seconds
    },
    'tickparser': {
        'size': 5,            # elements in array
        'rate_dev': 10, # Max deviation for adjacent ticks, i.e. abs((T2-T1)/T2)
        'max_dev': 150,
        'buffer_len': 10
    }
}


locators_v3 = {
    'LOGIN' : {
        'START': '//*[@id="page_home_index"]/header/div[2]/div/div/div[2]/div/button',
        'USER': '//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[1]/ui-n-input/div/input',
        'PASS': '//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[2]/ui-n-input/div/input',
        'SUBMIT': '//*[@id="sign-in"]/div/div/div[1]/ui-scroll/div/div/div/div/app-auth-signin/form/div[4]/button'
    },

    'CANVAS': {
        'EURUSD': '//*[@id="eur-graphs"]/div/div/ui-chart-container/div/div[2]/ui-chart/div[1]/canvas[1]',
        'USEROP': '//*[@id="eur-graphs"]/div/aside/div/div[1]/div/div[4]/div/span[2]'
    }
}


# Write to console log for other classes
class Logger(object):
    def __init__(self, prefix=None):
        self.prefix = "" if prefix is None else ' [{}]'.format(str(prefix))

    # Print with timestamp
    def log(self, text):
        print(datetime.now().strftime("[%d.%m-%H:%M:%S]{} {}".format(self.prefix, str(text))))

    # Print error with exception message and exit
    def fail(self, ex):
        print('\r\n\r\nERROR: {}'.format(str(ex)))
        sys.exit(666)

# Smart wait for ticker
class DataTimer(object):
    def __init__(self):
        self.params=params_v3['datatimer']
        self.timer=datetime.now()
        self.value = 0

    def wait(self):
        while self.params['enabled']:
            now = datetime.now()
            if (now-self.timer).total_seconds() > self.params['rate']:
                self.timer = now
                break
            time.sleep(self.params['nop'])

# Chromedriver browser
class Browser(object):
    def __init__(self):
        # Init Driver with options
        self.params=params_v3['browser']
        self.logger=Logger('CHROME')
        self.userop=None
        opt = wd.Options()
        if self.params['headless']:
            opt.add_argument('--headless')
        self.driver = Chrome(options=opt, executable_path=self.params['driver'])
        self.driver.set_window_size(self.params['window'][0], self.params['window'][1])
        self.logger.log('Initialized ' + os.path.basename(self.params['driver']) + ', headless=' +
              str(self.params['headless']))

    # Take screenshot from canvas + filter + OCR ==> text value of current rate
    def take_screen(self, debug=False):
        cfg = '--psm 8 -c tessedit_char_whitelist=0123456789. --user-patterns ' + self.params['pattern']

        # Need to locate each time to prevent glitches
        if not debug:
            png = self.driver.find_element_by_xpath(locators_v3['CANVAS']['EURUSD']).screenshot_as_png
            img = Image.open(BytesIO(png))
            img.save('test1.png')
        #else:
            #img = Image.open('test1.png')

        w, h = img.size
        i = img.crop((w-130, 0, w, h))
        i = i.convert('L')
        i = np.array(i)
        i = self.binarize_array(i)
        i = Image.fromarray(i)
        #i = i.filter(ImageFilter.SMOOTH)
        #i = i.filter(ImageFilter.UnsharpMask)
        #i = i.filter(ImageFilter.EDGE_ENHANCE)
        #i = i.filter(ImageFilter.SHARPEN)
        #i.save('test.png')

        if debug:
            from random import randint
            useropinion = randint(0, 99)
        else:
            useropinion = int(self.userop.get_attribute("innerHTML").replace(" ", "").replace("%", ""))
        realrate = pytesseract.image_to_string(i, config=cfg)
        realrate = int(realrate.replace(' ','').replace('.','').strip())

        if debug:
            print(realrate)

        return realrate, useropinion, datetime.now().second


    # Used for filtering
    def binarize_array(self, numpy_array, threshold=200):
        """Binarize a numpy array."""
        h_min = 0
        h_max = len(numpy_array)
        height = 20

        for i in range(len(numpy_array)):
            for j in range(len(numpy_array[0])):
                if numpy_array[i][j] > threshold:
                    numpy_array[i][j] = 255
                    if j > len(numpy_array[0]) / 2:
                        h_min = max(h_min, i - height)
                        h_max = min(h_max, i + height)
                else:
                    numpy_array[i][j] = 0

        for i in range(h_min):
            for j in range(len(numpy_array[0])):
                numpy_array[i][j] = 0
        for i in range(h_max, len(numpy_array)):
            for j in range(len(numpy_array[0])):
                numpy_array[i][j] = 0
        return numpy_array

    # Destructor
    def close_driver(self):
        self.driver.quit()
        self.logger.log('Closed ' + os.path.basename(self.params['driver']))

    # Press Button
    def btn_down(self, btn):
        actions = ActionChains(self.driver)
        actions.send_keys(btn)
        actions.perform()

    # Press Button
    def btn_down2(self, btn):
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL)
        actions.key_down('+')
        actions.key_up(Keys.CONTROL)
        actions.perform()

    # Login to binomo and get xPaths
    def init_binomo(self):
        self.driver.implicitly_wait(self.params['wait'])
        self.logger.log('Logging to Binomo')
        self.driver.get('https://binomo.com/ru/trading')
        time.sleep(5)
        self.driver.find_element_by_xpath(locators_v3['LOGIN']['START']).click()
        self.driver.find_element_by_xpath(locators_v3['LOGIN']['USER']).send_keys("sadxui@gmail.com")
        self.driver.find_element_by_xpath(locators_v3['LOGIN']['PASS']).send_keys("Htgbyj86")
        self.driver.find_element_by_xpath(locators_v3['LOGIN']['SUBMIT']).click()
        self.logger.log('Entering session')
        time.sleep(5)
        self.btn_down('z')
        try:
            self.userop = self.driver.find_element_by_xpath(locators_v3['CANVAS']['USEROP'])
        except Exception as e:
            self.logger.log('Initialization error, possible too many logins to Binomo, wait for 1h')
            self.close_driver()
            self.logger.fail(e)
        #self.driver.execute_script("document.body.style.zoom='110%'")
        self.btn_down2()
        self.logger.log('Init OK')

# Parse raw data
class TickParser(object):
    def __init__(self):
        self.params = params_v3['tickparser']
        self.logger = Logger('TPARSE')
        self.dt = DataTimer()
        self.b = Browser()
        self.data = None
        self.gsec = 0
        self.needupdate = False
        self.buffer = []

    # Add new non-deviated value with shift
    def checkadd(self, newvalue):
        newdata = None
        if self.gsec != newvalue[2]:
            newdata=np.roll(self.data, -1)
            newdata[self.params['size'] - 1] = newvalue[0]
        else:
            newdata = np.copy(self.data)
            newdata[self.params['size'] - 1] = int(round((newvalue[0] + newdata[self.params['size'] - 1])/2))

        old_dev = self.data.std()
        if old_dev == 0:
            old_dev = self.params['max_dev']

        if old_dev * self.params['rate_dev'] > newdata.std():
            self.data = newdata
            if self.gsec != newvalue[2]:
                self.logger.log('Add: {}'.format(str(newvalue)))
                self.gsec = newvalue[2]
                self.needupdate = False
            else:
                self.logger.log('Upd: {}'.format(str(newvalue)))
                self.needupdate = True
            return True
        else:
            self.logger.log('ERROR value: {}'.format(newvalue[0]))
            return False

    # Get tick value w/o noise
    def getlast(self):
        return int(round(np.mean(self.data)))

    # Initialize ticker by quorum of 3 values
    def initialize(self):
        self.logger.log('Ticker initialization')
        while True:
            data0 = self.b.take_screen()[0]
            time.sleep(1)
            data1 = self.b.take_screen()[0]
            time.sleep(1)
            data2 = self.b.take_screen()[0]

            if np.std(np.array([data0, data1, data2])) < self.params['max_dev']:
                self.data = np.array([data0] * self.params['size'])
                self.logger.log('Initialization OK: data0={:d}'.format(data0))
                break
            self.logger.log('Initialization failed, retrying'.format(data0))

    # Main program cycle
    def main(self):
        self.b.init_binomo()
        time.sleep(5)
        self.initialize()
        self.logger.log('Start main cycle')

        with open('out.txt', 'w') as f:
            while True:
                try:
                    data = self.b.take_screen()
                except Exception as e:
                    self.logger.log('ERROR taking screen: {}'.format(str(e)))
                    continue
                if self.checkadd(data):
                    gotdata=(self.getlast(), data[1], data[2])
                    if self.needupdate and len(self.buffer) > 0:
                        # Do not use [-1:]
                        self.buffer[len(self.buffer)-1] = gotdata
                    else:
                        self.buffer.append(gotdata)
                    if len(self.buffer) > self.params['buffer_len']:
                        self.logger.log('Saving data')
                        for item in self.buffer:
                            print('{:d},{:d},{:d}'.format(item[0], item[1], item[2]), file=f)
                        f.flush()
                        self.buffer = []
                    self.dt.wait()


tp = TickParser()
tp.main()


