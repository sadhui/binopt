

==============================
INPUT FOLDER: 	C:\PLAY\binopt\OCR\IN
Process images from subfolders
Continually check the folder for incoming images.
FILE OPTIONS: 	Create a separate document for each file (retains folder hierarchy)

OUTPUT FOLDER: 	C:\PLAY\binopt\OCR\OUT
SAVE AS TYPE: 	Text (*.txt)
------------------------------
09.02.2019, 18:35:28	Running...
09.02.2019, 18:35:28	Found 2 image files (2 pages). Processing....
09.02.2019, 18:35:28	Warning (C:\PLAY\binopt\OCR\IN\1.jpg, page 0): Increase resolution to 300 dpi or greater.
09.02.2019, 18:35:28	Warning (C:\PLAY\binopt\OCR\IN\2.jpg, page 0): Increase resolution to 300 dpi or greater.
09.02.2019, 18:35:29	Removing the processed images...
09.02.2019, 18:35:29	Removing the processed images...
09.02.2019, 18:35:29	Completed.
------------------------------
Pages processed: 	2.
Recognition time: 	0 hours 0 minutes 1 seconds.
Errors/warnings : 	0 / 2.
Low-confidence characters: 	7 % (4 / 54).
==============================